﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TMgenerator
{
    public class Automaton
    {
        private string Name  {get;set;}
        private string Blank  { get; set;}
        private  List<string> Finals { get; set; }
        private List<string> Alphabet { get; set; }
        private List<List<string>> Transitions { get; set; }
        private List<string> States { get; set; }
        private Head Head = null;

        public Automaton(string name, string blank, List<string> finals, List<string> alphabet, List<List<string>> transitions, List<string> states)
        {
            Name = name;
            Blank = blank;
            Finals = finals;
            Alphabet = alphabet;
            Transitions = transitions;
            States = states;
            this.Head = new Head();
        }
        public string getName()
        {
            return this.Name;
        }
        public string getBlank()
        {
            return this.Blank;
        }
        public List<string> getFinals()
        {
            return this.Finals;
        }
        public List<string>getAlphabeth()
        {
            return this.Alphabet;
        }
        public List<List<string>> getTransitions()
        {
            return this.Transitions;
        }
        public List<string>getStates()
        {
            return this.States;
        }
        public void InitAutomaton(string Entry)
        {
            
            int currentState = 0;
            int transitionCounter = 0;
            int index = 0;
            string[] currentTransition = null;
            string currentEntry = string.Empty;
            while (true)
            {
                //16 21 22 17
                Thread.Sleep(150);
                
                if (this.Finals.Contains(currentState.ToString())) {
                    string aux1 = ReplaceAt(index, Entry, currentTransition[1]);
                    Entry = aux1;
                    Console.Clear();
                    writeMachineState(currentTransition, currentState.ToString(), Entry, currentTransition[1], index,"_");
                    break;
                }
               
                try
                {
                    currentTransition = splitTransition(this.getTransitions().ElementAt(currentState)
                   .ElementAt(transitionCounter));
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("The input was rejected because there is no matching transition!");
                    break;
                }
                if (index < 0)
                {
                    string aux = "_" + Entry;
                    Entry = aux;
                    currentEntry = Entry[0].ToString();
                    index = 0;
                    
                }
                else
                {
                    if (index >= Entry.Length)
                    {
                        Entry += this.Blank;
                        currentEntry = Entry[index].ToString();
                    }
                    else
                    {
                        currentEntry = Entry[index].ToString();
                    }
                }
               
              
                if (transitionMatch(currentTransition, currentEntry))
                {
                    //evaluates the transition
                    
                    string aux1=ReplaceAt(index,  Entry, currentTransition[1]);
                    Entry = aux1;
                    Console.Clear();
                    writeMachineState(currentTransition,currentState.ToString(),Entry,currentEntry,index,currentTransition[2]);
                    if (currentTransition[2] != currentState.ToString())
                    {
                        //make the state change
                        currentState = int.Parse(currentTransition[2]);
                        transitionCounter = 0;
                    }
                    index += getDirection(currentTransition[3]);
                    
                }
                else
                {
                    transitionCounter++;

                }


            }
           
           
        }
        private void writeMachineState(string[] transition, string state,string Entry,string currentEntry,int index,string nextState)
        {
            Console.Write("Transition:{0}\tCurrent state:{1}\t\tNext state:{3}\t\tTape:{2}", string.Join(",",transition),state,Entry.Substring(0,index),nextState);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(currentEntry);
            Console.ResetColor();
            Console.Write(Entry.Substring(index + 1)+"\n");

        }
        public bool transitionMatch(string []transition, string entry)
        {
            
            return transition[0].Equals(entry);
        }

        private string ReplaceAt(int index,  string st,string newChar)
        {
            string aux = st;
            st = st.Substring(0, index);
            st += newChar;
            st += aux.Substring(index + 1);
            /* for(int i = 0; i < st.Length; i++)
             {
                 if (i.Equals(index))
                 {
                     if (newChar.Equals("B"))
                     {
                         newChar = this.Blank;
                     }
                     newSt += newChar;
                 }
                 newSt += st[i].ToString();
             }
             st = newSt;*/
            return st;
        }
        private string NextState(string currentEntry,string currentState,string currentTransition)
        {
            string []aux = splitTransition(currentTransition);
            if (currentEntry == aux[0])
            {
                return aux[2];
            }
            return "n";
            

        }
        public int getDirection(string direction)
        {
            return  (direction=="R")? 1 : (direction=="L")?-1:0;
        }
        public string[] splitTransition(string currentTransition)
        {
            currentTransition = currentTransition.Replace("(", "");
            currentTransition = currentTransition.Replace(")", "");
            string[] aux = currentTransition.Split(',');
            return aux;
        }
    }
}
