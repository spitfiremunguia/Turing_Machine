﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMgenerator
{
    public class Head
    {
        private string CurrentState { get; set; }
        private string CurrentEntry { get; set; }
        private string CurrentMove { get; set; }
        public Head()
        {
            
        }
        public void setCurrentState(string s)
        {
            this.CurrentState = s;
        }
        public void setCurrentEntry(string s)
        {
            this.CurrentEntry = s;
        }
        public void setCurrentMove(string s)
        {
            this.CurrentMove = s;
        }
        public string getCurrentState()
        {
            return this.CurrentState;
        }
        public string getCurrentEntry()
        {
            return this.CurrentEntry;
        }
        public string getCurrentMove()
        {
            return this.CurrentMove;
        }
        public void turnOnMacnine(string input, List<List<string>> states)
        {

        }
    }
}
