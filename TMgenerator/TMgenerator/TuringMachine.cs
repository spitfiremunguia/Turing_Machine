﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;


namespace TMgenerator
{
    public class TuringMachine
    {
        
        private String MachinePath = string.Empty;
        private List<Automaton> allAutomatons = null;
        public TuringMachine(String path)
        {
            this.MachinePath = path;
            this.allAutomatons = new List<Automaton>();
        }
        public void getAllMachines()
        {
            if (File.Exists(this.MachinePath))
            {
                string[] lines = File.ReadAllLines(this.MachinePath);
                string code = string.Join("\n", lines);
                string pattern = @"\[machine\]";
                string[] allCode = Regex.Split(code,pattern);
                List<Automaton> allAutomaton = new List<Automaton>();
                string statePattern = @"\{[0-9]+\}";
                foreach (string s in allCode)
                {
                    if (s.Equals(string.Empty)) continue;
                    string[] automatonCode = s.Split('\n');
                    List<string> automatonRealCode = new List<string>();
                    for (int i = 0; i < automatonCode.Length; i++) { if (!automatonCode[i].Equals(string.Empty)) { automatonRealCode.Add(automatonCode[i]); } }
                    string name = automatonRealCode.ElementAt(0);
                    List<string> alphabet = automatonRealCode.ElementAt(1).Split(',').ToList<string>();
                    string blank = automatonRealCode.ElementAt(2);
                    List<string> finals = automatonRealCode.ElementAt(3).Split(',').ToList<string>();
                    Regex innerRegex = new Regex(statePattern);
                    List<string> states = automatonRealCode.ElementAt(4).Split(',').ToList<string>();
                    List<string> transitions = innerRegex.Split(s).ToList<string>();
                    transitions.RemoveAt(0);
                    List<List<string>> allTransitions = new List<List<string>>();
                    foreach (string t in transitions)
                    {
                        List<string> currentTransition = t.Replace('\n', ';').Split(';').ToList<string>();
                        currentTransition.RemoveAll(item => item == "");
                        allTransitions.Add(currentTransition);
                        
                        
                    }
                    allAutomatons.Add(generateTuringMachine(name, blank, finals, alphabet, allTransitions, states));
                }
               
            }
            else
            {
                throw new FileNotFoundException("Machines descriptor file not found");
            }
        }
        public Automaton generateTuringMachine(string name, string blank, List<string> finals, List<string> alphabet, List<List<string>> transitions, List<string> states)
        {
            Automaton automaton = new Automaton(name,blank,finals,alphabet,transitions,states);
            return automaton;
        }
        public Automaton GetAutomaton(string automatonName)
        {
            return this.allAutomatons.Where(item => item.getName() == (automatonName)).FirstOrDefault();
        }
        public List<Automaton> getTheAutomatons()
        {
            return this.allAutomatons;
        }
        public void getAutomatonsNames()
        {
            foreach (Automaton a in this.allAutomatons)
            {
                Console.WriteLine(a.getName());
            }
        }
    }
}
