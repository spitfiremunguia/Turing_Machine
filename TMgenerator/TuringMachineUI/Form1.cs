﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TMgenerator;
using System.Threading;


namespace TuringMachineUI
{
    public partial class Form1 : Form
    {
        public TuringMachine tm = null;
        public Automaton currentAutomaton = null;
        public int stepCounter = 0;
        public string ms = "Steps:";
        Color defaultCellColor = Color.White;
        public Form1()
        {
            InitializeComponent();
            this.button2.Enabled = false;
        }
        /// <summary>
        /// Load the machines.txt to the projct
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Choose the file that contains the machines description";
            openFileDialog1.Filter = "TXT files|*.txt";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = openFileDialog1.FileName;
                this.tm = new TuringMachine(path);
                tm.getAllMachines();
                listBox1.Items.Clear();
                foreach (Automaton a in tm.getTheAutomatons())
                {
                    this.listBox1.Items.Add(a.getName());
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            
        }
        /// <summary>
        /// Fill the grids with the current machine description
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView2.RowHeadersVisible = false;
            dataGridView2.RowTemplate.Height = 50;

            Automaton currentAutomaton = tm.GetAutomaton(this.listBox1.SelectedItem.ToString());
            this.currentAutomaton = currentAutomaton;
            if (currentAutomaton != null)
            {
                dataGridView2.Columns.Clear();
                //cleanData(dataGridView2);
                int max = 0;
                for (int i = 0; i < currentAutomaton.getStates().Count; i++)
                {
                    this.dataGridView2.Columns.Add("", currentAutomaton.getStates().ElementAt(i));

                }
                populateRows(max, currentAutomaton);
                this.dataGridView2.Rows[0].Cells[0].Selected = false;


            }
            
        }
        private void populateRows(int max, Automaton currentAutomaton)
        {
            for (int i = 0; i < currentAutomaton.getTransitions().Count; i++)
            {
                if (currentAutomaton.getTransitions().ElementAt(i).Count > max)
                {
                    max = currentAutomaton.getTransitions().ElementAt(i).Count;
                }
            }
            for (int i = 0; i < max; i++)
            {
                dataGridView2.Rows.Add();
            }
            for (int i = 0; i < currentAutomaton.getTransitions().Count; i++)
            {
                int index = currentAutomaton.getTransitions().ElementAt(i).Count;
                for (int j = 0; j < index; j++)
                {

                    string transition = currentAutomaton.getTransitions().ElementAt(i).ElementAt(j);
                    dataGridView2.Rows[j].Cells[i].Value = transition;

                }
            }
        }
        private void cleanData(DataGridView dgbDatos)
        {
            int numRows = dgbDatos.Rows.Count;
            for (int i = 0; i < numRows; i++)
            {
                try
                {
                    int max = dgbDatos.Rows.Count - 1;
                    dgbDatos.Rows.Remove(dgbDatos.Rows[max]);

                }
                catch (Exception exe)
                {
                    MessageBox.Show("You can´t delete A row " + exe, "WTF",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        private void initializeTape()
        {
            int height = this.dataGridView1.Height;
            int width = this.dataGridView1.Width;
            string Entry = this.textBox1.Text;
            dataGridView1.RowTemplate.Height = height;
            dataGridView1.ColumnHeadersVisible = false;
            dataGridView1.RowHeadersVisible = false;
            // dataGridView1.Columns.Add("", "");
            //dataGridView1.Rows[0].Cells[0].Value = ("_");
            for (int i = 0; i < Entry.Length; i++)
            {
                dataGridView1.Columns.Add("", "");
                dataGridView1.Rows[0].Cells[i].Value = Entry[i].ToString();

            }
           // dataGridView1.Rows[0].Cells[0].Selected = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text == string.Empty)
            {
                return;
            }
            this.richTextBox1.Text = "";
            dataGridView1.Columns.Clear();
            initializeTape();
            
            this.button2.Enabled = true;

        }
        private void Analyze(string Entry)
        {
            int currentState = 0;
            int transitionCounter = 0;
            int index = 0;
            string[] currentTransition = null;
            label1.Text = ms;
            stepCounter = 0;
            string currentEntry = string.Empty;
            paintCell(index);

            while (true)
            {
                //16 21 22 17
                Thread.Sleep(500);
                if (this.currentAutomaton.getFinals().Contains(currentState.ToString()))
                {
                   
                    break;
                }

                try
                {
                    currentTransition = this.currentAutomaton.splitTransition(this.currentAutomaton.getTransitions().ElementAt(currentState)
                   .ElementAt(transitionCounter));
                }
                catch (ArgumentOutOfRangeException)
                {
                    MessageBox.Show("The input was rejected because there is no matching transition!");
                    break;
                }
                if (index < 0)
                {

                    DataGridViewColumn buttonColumn =
                    new DataGridViewColumn();
                    buttonColumn.Name = "";
                    buttonColumn.HeaderText = "";
                    buttonColumn.Visible = true;
                    buttonColumn.Width = dataGridView1.Columns[0].Width;
                    buttonColumn.CellTemplate = dataGridView1.Columns[0].CellTemplate;
                    // Add the button column to the control.
                    dataGridView1.Columns.Insert(0, buttonColumn);
                    dataGridView1.Refresh();
                    index = 0;
                    dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[index];
                    dataGridView1.Rows[0].Cells[index].Value = currentAutomaton.getBlank();
                    Entry += this.currentAutomaton.getBlank();
                    currentEntry =this.currentAutomaton.getBlank();



                }
                else
                {
                    if (index >= this.dataGridView1.Columns.Count)
                    {
                        //add a new columns to the tape
                        this.dataGridView1.Columns.Add("", "");
                        dataGridView1.Refresh();
                        dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[index];
                        dataGridView1.Rows[0].Cells[index].Value = currentAutomaton.getBlank();
                        Entry += this.currentAutomaton.getBlank();
                        currentEntry = Entry[index].ToString();
                    }
                    else
                    {
                        currentEntry = this.dataGridView1.Rows[0].Cells[index].Value.ToString();//Entry[index].ToString();
                    }
                }
                paintCell(index);

                if (this.currentAutomaton.transitionMatch(currentTransition, currentEntry))
                {
                    //evaluates the transition
                    ReplaceAt(index, currentTransition[1]);
                    paintTransition(currentState,transitionCounter);
                    stepCounter++;
                    this.label1.Text = ms + stepCounter.ToString();
                    label1.Refresh();
                    writeMovement(string.Join(",", currentTransition),currentEntry);
                    //writeMachineState(currentTransition, currentState.ToString(), Entry, currentEntry, index, currentTransition[2]);
                    if (currentTransition[2] != currentState.ToString())
                    {
                        //make the state change
                        currentState = int.Parse(currentTransition[2]);
                        transitionCounter = 0;
                    }
                    index += currentAutomaton.getDirection(currentTransition[3]);

                }
                else
                {
                    List<string> st = this.currentAutomaton.getTransitions().ElementAt(currentState);
                    bool flag = true;
                    for (int i = 0; i < st.Count; i++)
                    {
                        string[] transition = this.currentAutomaton.splitTransition(st.ElementAt(i));
                        if (this.currentAutomaton.transitionMatch(transition, currentEntry))
                        {
                            flag = false;
                            transitionCounter = i;
                            break;
                        }
                    }
                    if (flag) {
                        MessageBox.Show("The input was rejected because there is no matching transition!");
                        break;
                    }

                }


            }
        }
        private void ReplaceAt(int index, string newEntry)
        {
            dataGridView1.Rows[0].Cells[index].Value = newEntry;
        }
        private void paintCell(int index)
        {
            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.White;
            }
            dataGridView1.CurrentCell = dataGridView1.Rows[0].Cells[index];
            dataGridView1.RowsDefaultCellStyle.SelectionBackColor = Color.Yellow;
            /*dataGridView1.Rows[0].Cells[index].Style.BackColor = Color.Yellow;*/
            dataGridView1.Refresh();
        }
        private void paintTransition(int state,int index)
        {
            for (int i = 0; i < dataGridView2.Columns.Count; i++)
            {
                for (int j = 0; j < dataGridView2.Rows.Count; j++)
                {
                    dataGridView2.Rows[j].Cells[i].Style.BackColor = Color.White;

                }
            }
            dataGridView2.CurrentCell = dataGridView2.Rows[index].Cells[state];
            dataGridView2.Rows[index].Cells[state].Style.BackColor = Color.Yellow;
            dataGridView2.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.currentAutomaton == null)
            {
                return;
            }
            Analyze(this.textBox1.Text);
            this.dataGridView1.Rows[0].Cells[0].Selected = false;
            this.button2.Enabled = false;
        }
        private void writeMovement(string transition,string newEntry)
        {
            string[] st = transition.Split(',')
                ;
            this.richTextBox1.Text += "Current Entry: "+newEntry+"\t"+"Current transition: ("+transition+") \tEstate:"+st[2]
            +"\n";
            this.richTextBox1.Refresh();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
            // scroll it automatically
            richTextBox1.ScrollToCaret();
        }
    }
}
