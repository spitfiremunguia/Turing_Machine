﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMgenerator;

namespace tmTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the path of the machines.txt");
            string path = Console.ReadLine();
            TuringMachine tm = new TuringMachine(path);
            tm.getAllMachines();
            string entry = "111-11";
            tm.GetAutomaton("sub").InitAutomaton(entry);
            Console.ReadLine();
        }
    }
}
