[machine]
sum
1,+,=
B
3
0,1,2,3
{0}
(1,1,0,R)
(+,1,1,R)
{1}
(1,1,1,R)
(B,B,2,L)
{2}
(1,B,3,I)
[machine]
sub
1,-
B
8,9
0,1,2,3,4,5,6,7,8,9
{0}
(1,1,0,R)
(-,-,1,R)
{1}
(1,1,1,R)
(B,B,2,L)
{2}
(1,B,3,L)
(-,B,8,I)
{3}
(1,1,3,L)
(-,-,4,L)
{4}
(B,B,4,L)
(1,B,5,R)
(_,_,6,R)
{5}
(B,B,5,R)
(-,-,1,R)
{6}
(B,B,6,R)
(-,-,7,R)
(1,1,6,R)
{7}
(1,1,7,R)
(B,1,9,I)
[machine]
mult
1,*,X
B
8
0,1,2,3,4,5,6,7,8
{0}
(*,B,7,R)
(1,B,1,R)
{1}
(1,1,1,R)
(*,*,2,R)
{2}
(X,X,2,R)
(1,X,3,R)
(=,=,6,L)
{3}
(1,1,3,R)
(=,=,4,R)
{4}
(1,1,4,R)
(B,1,5,L)
{5}
(1,1,5,L)
(=,=,5,L)
(X,X,2,R)
{6}
(X,1,6,L)
(*,*,6,L)
(1,1,6,L)
(B,B,0,R)
{7}
(1,B,7,R)
(=,B,8,I)
[machine]
copy
a,b,c,X,Y,Z,.
B
12
0,1,2,3,4,5,6,7,8,9,10,11,12
{0}
(a,a,0,R)
(b,b,0,R)
(c,c,0,R)
(B,.,1,L)
{1}
(a,a,1,L)
(b,b,1,L)
(c,c,1,L)
(X,X,1,L)
(Y,Y,1,L)
(Z,Z,1,L)
(B,B,2,R)
{2}
(X,X,2,R)
(Y,Y,2,R)
(Z,Z,2,R)
(a,X,3,R)
(b,Y,5,R)
(c,Z,6,R)
(.,.,7,R)
{3}
(a,a,3,R)
(b,b,3,R)
(c,c,3,R)
(.,.,3,R)
(B,a,4,L)
{4}
(a,a,4,L)
(b,b,4,L)
(c,c,4,L)
(.,.,1,L)
{5}
(a,a,5,R)
(b,b,5,R)
(c,c,5,R)
(.,.,5,R)
(B,b,4,L)
{6}
(a,a,6,R)
(b,b,6,R)
(c,c,6,R)
(.,.,6,R)
(B,c,4,L)
{7}
(a,.,8,L)
(b,.,9,L)
(c,.,10,L)
(.,.,7,R)
(B,B,11,L)
{8}
(.,a,7,R)
{9}
(.,b,7,R)
{10}
(.,c,7,R)
{11}
(.,B,11,L)
(a,a,11,L)
(b,b,11,L)
(c,c,11,L)
(X,a,11,L)
(Y,b,11,L)
(Z,c,11,L)
(B,B,12,I)
[machine]
palindrome
a,b,c
B
8
0,1,2,3,4,5,6,7,8
{0}
(a,B,1,R)
(b,B,2,R)
(c,B,3,R)
(B,B,8,I)
{1}
(a,a,1,R)
(b,b,1,R)
(c,c,1,R)
(B,B,4,L)
{2}
(a,a,2,R)
(b,b,2,R)
(c,c,2,R)
(B,B,5,L)
{3}
(a,a,3,R)
(b,b,3,R)
(c,c,3,R)
(B,B,6,L)
{4}
(a,B,7,L)
(B,B,8,I)
{5}
(b,B,7,L)
(B,B,8,I)
{6}
(c,B,7,L)
(B,B,8,I)
{7}
(a,a,7,L)
(b,b,7,L)
(c,c,7,L)
(B,B,0,R)
